const fs = require("fs");
const express = require("express");
const app = express();
const port = 3001;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get("/copic/parse", (req, res) => {
    fs.readFile("data/copic-raw.json", "utf-8", (err, data) => {
        if (err) {
            console.error(err);
            res.send("Something happened!");
        } else {
            const file = JSON.parse(data);
            const colors = file.colors;
            let rgbColors = {};
            console.log(`Looping over ${colors.length} colors...`);
            colors.forEach((color, i) => {
                rgbColors[color.name] = {
                    r: Math.round(color.red * 255),
                    g: Math.round(color.green * 255),
                    b: Math.round(color.blue * 255)
                };
            });

            const rgbJSON = JSON.stringify(rgbColors);

            fs.writeFile("data/copic-rgb.json", rgbJSON, writeErr => {
                if (writeErr) {
                    console.error(writeErr);
                    res.send("Something happened!");
                } else {
                    console.log("Wrote colors-rgb.json");
                    res.send(rgbJSON);
                }
            });
        }
    });
});

app.get("/copic/:color", (req, res) => {
    const color = req.params.color;
    fs.readFile("data/copic-rgb.json", (readErr, readData) => {
        if (readErr) {
            console.error(readErr);
            res.statusCode(500);
            res.send("Something went wrong!");
        } else {
            const colors = JSON.parse(readData);
            const nearestColor = require("nearest-color").from(colors);
            const commaSplit = color.split(",");
            const numCommas = commaSplit.length - 1;
            let copicColor;
            if (numCommas === 2) {
                copicColor = nearestColor({ 
                    r: commaSplit[0],
                    g: commaSplit[1],
                    b: commaSplit[2]
                });
            } else if (numCommas === 0 && (color.length === 6 || color.length === 3)) {
                copicColor = nearestColor(`#${color}`);
            } else {
                res.send("Unknown color format. Please send either a hex or an rgb color");
            }
            if (copicColor) {
                res.send(copicColor);
            }
        }
    })
});

app.listen(port, () => console.log("Copic Colors is listening"))

